import React, { Component } from 'react';

/*An unlogged user can research and view all the artists and locals pages, and the events*/

class UnloggedSearch extends Component {

    render(){
        return(
            <div className="container-fluid">
                <div className="row justify-content-center" >
                    <div className="col-xs-12 col-md-8 " style={{position: "absolute",top: "50%"}}>
                        <div className="input-group mb-3">
                            <input type="text" className="form-control" placeholder="Ricerca un artista, un locale, un evento..." aria-label="Search by keywords artist local or event" aria-describedby="basic-addon2" />
                            <div className="input-group-append">
                                <button className="btn btn-outline-secondary" type="button">Ricerca</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }




}

export default UnloggedSearch;