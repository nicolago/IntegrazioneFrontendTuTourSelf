import React, { Component } from 'react';
import {Link} from "react-router-dom";
import logo from '../flatLogo.png'

class Appbar extends Component{

    constructor() {
        super();

        this.state={
            links: {
                link1: "white",
                link2: "white",
                link3: "white"
            }
        };
    }
    componentDidMount(){
        console.log(this.props.location);
        if(this.props.location.pathname=="/signin")
        {

            this.handleClick(2);
        }
        if(this.props.location.pathname=="/signup")
        {
            this.handleClick(3);
        }
        if(this.props.location.pathname=="/home")
        {
            this.handleClick(1);
        }
    }

    //This handler change dynamically color to the links according to the current page 
    handleClick(num){
        switch(num){
            case 1:
                this.setState({
                    links:{
                        link1:"black",
                        link2:"white",
                        link3:"white"
                    }
                });
                break; 
            case 2:
                this.setState({
                    links:{
                        link1:"white",
                        link2:"black",
                        link3:"white"
                    }
                });
                break; 
            case 3:
                this.setState({
                    links:{
                        link1:"white",
                        link2:"white",
                        link3:"black"
                    }
                });
                break; 
        }  

        const element = document.getElementById("navbarNavAltMarkup");
        element.className = element.className.replace(/\bshow\b/g, "");
    }

    render(){
        return (
            <nav className="navbar navbar-expand-sm navbar-light bg-success justify-content-between text-white">

                     <Link to="/home" onClick={() => this.handleClick(1)} style={{color:this.state.links.link1}} className="nav-item nav-link navbar-brand" >
                    <img src={logo} width="60" height="60" alt="Logo TuTourSelf"/></Link>

                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div style={{justifyContent:"flex-end"}} className="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div style={{alignItems:"flex-end"}} className="navbar-nav">
                        <Link to="/home" onClick={() => this.handleClick(1)} style={{color:this.state.links.link1}} className="nav-item nav-link" >Explore</Link>
                        <Link to="/signin" onClick={() => this.handleClick(2)} style={{color:this.state.links.link2}} className="nav-item nav-link" >Sign In</Link>
                        <Link to="/signup" onClick={() => this.handleClick(3)} style={{color:this.state.links.link3}} className="nav-item nav-link" >Sign Up</Link>
                    </div>
                </div>
            </nav>
        );
    }
}

export default Appbar;