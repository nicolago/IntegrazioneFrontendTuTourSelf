import React, { Component } from 'react'
import {Link} from "react-router-dom";
import SpectatorSignup from './signupForms/SpectatorSignup';

class Signup extends Component {
    constructor(){
        super();
        this.state = {
           userType: "spectator"
        };
    }
    //This handler change dynamically color to the links according to the current page
    handleChange(e) {
        switch (e.target.value) {
            case 'spectator':
                this.setState({
                    userType: "spectator"
                });
                break;
            case 'artist':
                this.setState({
                    userType: "artist"
                });
                break;
            case 'localmanager':
                this.setState({
                    userType: "localmanager"
                });
                break;
        }
        console.log(this.state.userType);
    }

    handleUserForm(e) {
        const userType = this.state.userType;
        console.log(userType);
        //this.props.selectUserForm = userType;
        e.preventDefault();
    }


        render() {
    return (
        <div className="card text-center" style={{height:"30%",width:"50%", backgroundColor:"white"}}>
            <div className="card-header" style={{fontSize:"20px"}}>
                <p className="d-flex justify-content-around">Sign up</p>
            </div>
            <div className="card-body">
                <p className="card-text">Join TuTourSelf now!</p>
            </div>


            <form>
                <div className="form-group">
                    <label htmlFor="userTypeSelect" className="bmd-label-floating">Signup as...</label>
                    <select className="form-control" id="userTypeSelect" onChange={this.handleChange.bind(this)}>
                        <option value="spectator">Spectator</option>
                        <option value="artist">Artist</option>
                        <option value="localmanager">Local Manager</option>
                    </select>
                </div>
                <button className="btn btn-primary" onClick={this.handleUserForm.bind(this)}>Go ahead</button>

            </form>
        </div>
    )
  }
}

export default Signup;