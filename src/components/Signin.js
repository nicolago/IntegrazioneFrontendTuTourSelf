import React, { Component } from 'react'
import './Style.css';

export default class Signin extends Component {
  render() {
    return (
        <div className="card text-center" style={{height:"30%",width:"50%"}}>
            <div className="card-header" style={{fontSize:"20px"}}>
                <p className="d-flex justify-content-around">Sign in</p>
                <div className="d-flex justify-content-around">
                    <i className="fa fa-facebook" aria-hidden="true" style={{color:"#4267b3",marginRight:"20px",fontSize:"20px"}}/>
                    <i className="fa fa-google" style={{
                        color:"#eb4535",
                        fontSize:"20px"
                    }}></i>
                </div>
            </div>
            <div className="card-body">
                <p className="card-text">Sign in with your TuTourSelf account</p>


                    </div>


                <form>
                    <div className="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email"/>
                    </div>
                    <div className="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password"/>
                    </div>
                    <a href="#" className="btn btn-primary">Login</a>

                </form>
            </div>




    )
  }
}
