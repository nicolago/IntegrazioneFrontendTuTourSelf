import React, { Component } from 'react';
import { Route } from "react-router-dom";
import Appbar from './Appbar';
import UnloggedSearch from './UnloggedSearch';
import Signin from './Signin';
import Signup from './Signup';

class SigninSignupSearch extends Component{
    /*
    after retrieving user data from server this method sets userdata for the current logged user and redirects the user in his home page
     */
    retrieveData(userData){
        this.props.userData(userData);
        console.log(userData);
        this.props.history.push("/user");
    }
    /*
    this method displays the divs and checks the url for redirecting in register page or in login page
     */
    render(){
        return(
            <div>
                <Route path="/" render={(routeProps) => <Appbar {...routeProps}></Appbar>}/>
                <Route path="/signin" render={(routeProps) =>
                    <Signin {...routeProps}
                                    userData={this.retrieveData.bind(this)}>
                    </Signin>} />

                <Route path="/signup" render={(routeProps) =>
                <Signup {...routeProps}
                                userData={this.retrieveData.bind(this)}>
                </Signup>} />
                <Route path="/home" render={
                    () => <UnloggedSearch ></UnloggedSearch>
                }/>

                
            </div>
        );
    }

}

export default SigninSignupSearch;