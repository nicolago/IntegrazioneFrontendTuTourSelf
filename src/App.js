import React, { Component } from 'react';
import './style.css';
import { BrowserRouter as Router, Route, Switch} from "react-router-dom";
import UserInterface from './components/UserInterface';
import SigninSignupSearch from './components/SigninSignupSearch';

class App extends Component {

    /* The constructor set the initial state of App component */
    constructor(){
        super();
        this.state = {
            userData: ""
        };
    }

    /* This method sets the state of userData according to the data retrieved from the SignIn */
    retrieveData(data){
        this.setState({userData:data});
    }

    /* This is the render method that displays the component.
        There are two main routes: Login_Registra to retrieve the user data,
        while UserInterface defines which UI to display, using the data that are passed by App.js state.
    */
    render() {
        return (
            <Router>
                <Switch>
                    <Route path="/user" render={() =>
                        <UserInterface userData={this.state.userData}></UserInterface>} />
                    <Route path="/" render={(routeProps) =>
                        <SigninSignupSearch {...routeProps}
                                      userData={this.retrieveData.bind(this)}>

                        </SigninSignupSearch>} />
                </Switch>
            </Router>
        );
    }
}


export default App;